var express = require('express');
var app = express();
var fs = require("fs");
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(express.static('../gentelella-master'));

var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://127.0.0.1:27017/myNewDatabase';
// var ur2 = 'mongodb://admin:admin1234@ds163826.mlab.com:63826/markers';
var ur2 = 'mongodb://admin:admin123@ds151820.mlab.com:51820/crystaldb';
var db;
var dbName;
MongoClient.connect(ur2, function (err, dab) {
  if (err) {
    console.log('Unable to connect to the mongoDB server. Error:', err);
  } else {
    console.log('Connection established to', url);
    console.log(__dirname);
    var server = app.listen(process.env.PORT || 8087, function () {
      var host = server.address().address
      var port = server.address().port
      console.log("Example app listening at http://%s:%s", host, port)
    });
    db=dab;
      //Close connection
      //db.close();
  }
});

app.get('/', function (req, res) {
res.end('Welcome To My Restful Webservice');
});



app.post('/fetchContestnts', urlencodedParser, function (req, res) {
  console.log('Now Fetching All Contestants');
  var oprtnStatus;
  var myColl = db.collection('contestntsTbl');
    myColl.find().toArray(function (err, result) {
        if (err) {
        console.log(err);
        oprtnStatus = 'Could not Get Response';
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }else{
      var resltObj = result;
      console.log(resltObj.length);
      if(resltObj.length == 0){
        oprtnStatus = 'No Record';
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }else{
        oprtnStatus = JSON.stringify(resltObj);
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }
      }
      });
  
  });

app.post('/saveRoleDetails', urlencodedParser, function (req, res) {
    // Prepare output in JSON format
    console.log('Now Adding to Role Table');
    var status=' Failed';
    
    var tblData = req.query.postData;
    var tableValObj = JSON.parse(req.query.postData);
    var myColl = db.collection("rolesTbl");
    console.log(tblData); 
    myColl.insert([tableValObj], function (err, result) {
        if (err) {
        console.log(err);
        } else {
        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
        status = ' Successfull';
        }	
    res.end(status);
    });
    
});

app.get('/fetchAllRoles', urlencodedParser, function (req, res) {
  console.log('Now Fetching All Roles');
  var oprtnStatus;
  var myColl = db.collection('rolesTbl');
    myColl.find().toArray(function (err, result) {
        if (err) {
        console.log(err);
        oprtnStatus = 'Could not Get Response';
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }else{
      var resltObj = result;
      console.log(resltObj.length);
      if(resltObj.length == 0){
        oprtnStatus = [];
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }else{
        oprtnStatus = JSON.stringify(resltObj);
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }
      }
      });
  
});

app.post('/saveVoterDetails', urlencodedParser, function (req, res) {
    // Prepare output in JSON format
    console.log('Now Creating Collection Data');
    var status=' Failed';
    
    var tblData = req.query.postData;
    var tableValObj = JSON.parse(req.query.postData);
    var myColl = db.collection("contestntsTbl");
    console.log(tblData); 
    myColl.insert([tableValObj], function (err, result) {
        if (err) {
        console.log(err);
        } else {
        console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
        status = ' Successfull';
        }	
    res.end(status);
    });
    
});

app.post('/addContestantDetails', urlencodedParser, function (req, res) {
  // Prepare output in JSON format
  console.log('Now Creating Collection Data');
  var status=' Failed';
  
  var tblData = req.query.postData;
  var tableValObj = JSON.parse(req.query.postData);
  var myColl = db.collection("contestntsTbl");
  console.log(tblData); 
  myColl.insert([tableValObj], function (err, result) {
      if (err) {
      console.log(err);
      } else {
          var votesrecord = {Role:tableValObj.roles,contestantID:result._id,voteCount:0};
          myColl.insert([votesrecord],function(err2,res2){
            if(err2){
              console.log(err2);
            }else{
              console.log(res2);
              console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
              status = ' Successfull';
            }
          })
      }	
  res.end(status);
  });
  
})


app.post('/saveContDetails', urlencodedParser, function (req, res) {
  // Prepare output in JSON format
  console.log('Now Creating Collection Data');
  var status=' Failed';
  
  var tblData = req.query.postData;
  console.log(tblData); 
  var tableValObj = JSON.parse(tblData);
  var myColl = db.collection("contestntsTbl");
  for(i = 0; i < tableValObj.length; i++) {
    var roleType = tableValObj[i].roles;
    myColl.find({roles:roleType,contestantID:candidateID}).toArray(function (err, result) {
      if (err) {
            console.log(err);
            oprtnStatus = 'Could not Get Response';
            console.log(oprtnStatus);
            res.end(oprtnStatus);
      }else{
            var contestantRow = result;
            contestantRow.votes =  contestantRow.votes + 1;
            console.log(contestantRow.votes);
            myColl.update({votes:contestantRow.votes},function(err2,res2){
                if(err2){
                  console.log(err);
                }else{
                  console.log(res2);
                }
            });
            
      }
    });
  }
  
})



app.post('/fetchVote', urlencodedParser, function (req, res) {
  console.log('Now Fetching Vote Data');
  var oprtnStatus;
  var myColl = db.collection('voteTbl');
    myColl.find().toArray(function (err, result) {
        if (err) {
        console.log(err);
        oprtnStatus = 'Could not Get Response';
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }else{
      var resltObj = result;
      console.log(resltObj.length);
      if(resltObj.length == 0){
        oprtnStatus = 'No Record';
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }else{
        oprtnStatus = JSON.stringify(resltObj);
        console.log(oprtnStatus);
        res.end(oprtnStatus);
      }
      }
      });
  
  })

app.post('/saveVote', urlencodedParser, function (req, res) {
  // Prepare output in JSON format
  console.log('Now Creating Voter Data');
  var status=' Failed';
  
  var tblData = req.query.CastedVote;
  var tableValObj = JSON.parse(req.query.CastedVote);
  var myColl = db.collection("voteTbl");
  console.log(tblData); 
  myColl.insert(tableValObj, function (err, result) {
      if (err) {
      console.log(err);
      } else {
      console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result.length, result);
      status = ' Successfull';
      }	
  res.end(status);
  });
  
})

app.post('/deletParticipant', urlencodedParser, function (req, res) {
  // Prepare output in JSON format
  console.log('****Preparing to Delete Collection Data*****');
  var status=' Failed to Delete';
  var rowtoDelete = req.query.contestnID;
  console.log("Collection to Delete is: "+rowtoDelete);
  
    var myColl = db.collection("contestntsTbl");
  
    myColl.remove({rownum:rowtoDelete}, function (err, result) {
    if (err) {
    console.log(err);
    res.end(status);
    } else {
    //console.log('Deleted %d documents from the collection. The documents deleted with "_id" are:', result.length, result);
    status = 'ok';
    console.log(status);
    res.end(status);
    }
    //Close connection
    //db.close();
    });  
}) 

app.post('/checkUser', urlencodedParser, function (req, res) {
    
    console.log('Now Checking User');
    var oprtnStatus;
    var username = req.query.userName;
    var myColl = db.collection('userRecord');
    
      myColl.find({username:username}).toArray(function (err, result) {
          if (err) {
          console.log(err);
          oprtnStatus = 'Could not Get Response';
          console.log(oprtnStatus);
          res.end(oprtnStatus);
        }else{
        var resltObj = result;
        console.log(resltObj.length);
        oprtnStatus = 'No Record';
        for(i = 0; i < resltObj.length; i++) {
          if(username == resltObj[i].username){
            console.log(resltObj[i]);
            oprtnStatus = 'User Found';
            break;
          }		
        }
        console.log(oprtnStatus);
        res.end(oprtnStatus);
        }
        });
    
  })

  app.post('/signIn', urlencodedParser, function (req, res) {
    
    console.log('Now Signing In User');
    var oprtnStatus;
    var username = req.query.userName;
    var pass = req.query.passWord;
    console.log(pass);
    var myColl = db.collection('userRecord');
    
      myColl.find({username:username},{password:pass}).toArray(function (err, result) {
          if (err) {
          console.log(err);
          oprtnStatus = 'Could not Get Response';
          console.log(oprtnStatus);
          res.end(oprtnStatus);
        }else{
        var resltObj = result;
        console.log(resltObj.length);
        oprtnStatus = 'Invalid Password';
        for(i = 0; i < resltObj.length; i++) {
          if(pass == resltObj[i].password){
            console.log(resltObj[i]);
            oprtnStatus = 'Password Match';
            break;
          }		
        }
        if(oprtnStatus == 'Password Match'){
        var dbPassed = username + "Coll";
        dbName = dbPassed;
        res.end(oprtnStatus);
        }else{
        console.log(oprtnStatus);
        res.end(oprtnStatus);
        }
        
        }
        });
    
  })
    
  app.post('/signUp', urlencodedParser, function (req, res) {
    
    console.log('Now Adding User');
    var oprtnStatus;
    var username = req.query.userName;
    var pass = req.query.passWord;
    var collName = username + 'Coll';
    var userDetails = '{"username":"'+username+'","password":"'+pass+'","dbName":"'+collName+'"}';
    console.log(userDetails);
    userDetails = JSON.parse(userDetails);
    var myColl = db.collection('userRecord');
    
      myColl.insert([userDetails], function (err2, result2) {
            if (err2) {
            console.log(err2);
            oprtnStatus = err2;
            res.end(oprtnStatus);
            } else {
            console.log('Inserted %d documents into the "users" collection. The documents inserted with "_id" are:', result2.length, result2);
            oprtnStatus = 'Successfull';
            console.log(oprtnStatus);
            var dbPassed = username + "Coll";
            dbName = dbPassed;
            res.end(oprtnStatus);
            }	
            
            });
    
  })

  app.post('/logOut', urlencodedParser, function (req, res) {
    console.log('Now Loging Out Present User');
    db.close();
    res.end("");
  })


  app.get('/login.html', function (req, res) {
	//console.log(req);
	var redirectVal = __dirname + "/pages/" + "login.html";
	res.sendFile(redirectVal);
});

app.get('/profile.html', function (req, res) {
	//console.log(req);
	var redirectVal = __dirname + "/pages/" + "profile.html";
	res.sendFile(redirectVal);
});

app.get('/audit.html', function (req, res) {
	//console.log(req);
	var redirectVal = __dirname + "/pages/" + "audit.html";
	res.sendFile(redirectVal);
});

app.get('/manageroles.html', function (req, res) {
	//console.log(req);
	var redirectVal = __dirname + "/pages/" + "manageroles.html";
	res.sendFile(redirectVal);
});

// 

app.get('/addContestants.html', function (req, res) {
	//console.log(req);
	var redirectVal = __dirname + "/pages/" + "addContestants.html";
	res.sendFile(redirectVal);
});

app.get('/settings.html', function (req, res) {
	//console.log(req);
	var redirectVal = __dirname + "/pages/" + "settings.html";
	res.sendFile(redirectVal);
});


