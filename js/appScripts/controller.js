var app = angular.module('e-voting', []);
app.service("evotingService",function($http){
	var sheetList=[];
	var responseChunk=[];
	var user="";
	var userList = [];
	var sheetData=[];
	var dbCred;
	var sheetDataTypes=[
	{"fieldName":"Name","type":"Text","Mandate":"Y","Unq":"Y","value":"23"},
	{"fieldName":"Age","type":"Text","Mandate":"Y","Unq":"Y","value":"23"}
	];
	return {
		fetchdataTypes: function(){
		return sheetDataTypes;
		},	
		setDbCredentials: function(dbName){
		dbCred = dbName;
		//alert("set here is "+dbCred);
		
		},
		saveCastedVote: function (CastedVote) {
			//alert(postData);
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			if(this.responseText!=null ){
				user = this.responseText;
				alert(user);
			}else{
				alert("Failed");
			}
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/saveVote?CastedVote="+CastedVote, false);
			xhttp.send();
		return sheetList;
		},
		saveContDetails: function (postData) {
			//alert(postData);
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			if(this.responseText!=null ){
				user = this.responseText;
				//alert(user);
			}else{
				alert("Failed");
			}
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/saveContDetails?postData="+postData, false);
			xhttp.send();
		return sheetList;
		},
		saveRole: function (postData) {
			var postData = JSON.stringify(postData);
			alert(postData);
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			if(this.responseText!=null ){
				user = this.responseText;
				//alert(user);
			}else{
				alert("Failed");
			}
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/saveRoleDetails?postData="+postData, false);
			xhttp.send();
		return sheetList;
		},
		fetchUserList: function () {
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			if(this.responseText!=null ){
				userList = JSON.parse(this.responseText);
			}else{
				alert("Failed");
			}
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/fetchUsers", false);
			xhttp.send();
		return userList;
		},
		fetchUserDetail: function () {
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
			if(this.responseText!=null ){
				user = this.responseText;
			}else{
				alert("Failed");
			}
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/fetchSngUser", false);
			xhttp.send();
		return user;
		},
		fetchAllContestnts: function () {
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText!=null ){
					responseChunk = JSON.parse(this.responseText);
					//alert(user);
				}else{
					alert("Failed");
				}
			}};
			xhttp.open("post", "http://127.0.0.1:8081/fetchContestnts", false);
			xhttp.send();
		return responseChunk;
		},
		fetchRoles: function () {
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText!=null ){
					responseChunk = JSON.parse(this.responseText);
					//alert(user);
				}else{
					alert("Failed");
				}
			}};
			xhttp.open("get", "http://127.0.0.1:8081/fetchAllRoles", false);
			xhttp.send();
		return responseChunk;
		},
		fetchVoteResult: function () {
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText!=null ){
					responseChunk = JSON.parse(this.responseText);
					//alert(user);
				}else{
					alert("Failed");
				}
			}};
			xhttp.open("post", "http://127.0.0.1:8081/fetchVote", false);
			xhttp.send();
		return responseChunk;
		},
		deleteContestant: function (contestnID) {
			alert("in here "+ contestnID);
						var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText!=null ){
					responseChunk = JSON.stringify(this.responseText);
					alert(responseChunk);
				}else{
					alert("Failed");
				}
			}};
			xhttp.open("post", "http://127.0.0.1:8081/deletParticipant?partcpntID="+contestnID, false);
			xhttp.send();
		return responseChunk;
		},
		SignUp: function (username,pass) {
			//alert(username +" "+pass);
			var oprtnStatus = "Failed";
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText!=null ){
				oprtnStatus = this.responseText;
				}
			}else{
				oprtnStatus = "Failed to get Response from Server";
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/checkUser?userName="+username, false);
			xhttp.send();
				if(oprtnStatus == "No Record"){
					xhttp = new XMLHttpRequest();
					xhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if(this.responseText!=null ){
						oprtnStatus = this.responseText;
						}
					}else{
						oprtnStatus = "Failed to get Response from Server";
					}
					};
					xhttp.open("post", "http://127.0.0.1:8081/signUp?userName="+username+"&passWord="+pass, false);
					xhttp.send();
				}
			return oprtnStatus;
		},
		SignIn: function (username,pass) {
			//alert(username +" "+pass);
			var oprtnStatus = "Failed";
			var xhttp;  
			var xhttp;      
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				if(this.responseText!=null ){
				oprtnStatus = this.responseText;
				
				}
			}else{
				oprtnStatus = "Failed to get Response from Server 1";
			}
			};
			xhttp.open("post", "http://127.0.0.1:8081/checkUser?userName="+username, false);
			xhttp.send();
				if(oprtnStatus != "No Record"){
					xhttp = new XMLHttpRequest();
					xhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						if(this.responseText!=null ){
						oprtnStatus = this.responseText;
						}
					}else{
						oprtnStatus = "Failed to get Response from Server 2";
					}
					};
					xhttp.open("post", "http://127.0.0.1:8081/signIn?userName="+username+"&passWord="+pass, false);
					xhttp.send();
				}
				return oprtnStatus;
		}
	}
	
});




app.controller('e-votingCntrller', function ($scope,evotingService) {
	
	
	$("#ContestntList").show();;
	$("#ContestntDetails").hide();;
	$("#addNewContestnt").hide();;

	$scope.voteCount={count:0};
	$scope.contestRoles=[
	{"roles":"President"},{"roles":"V-President"},{"roles":"PRO"},{"roles":"Secetary"},
	{"roles":"Welfare"},{"roles":"Sport Director"}
	];

	$scope.votedFor=[
		{"roles":"President","candidateID":""},{"roles":"V-President","candidateID":""},
		{"roles":"PRO","candidateID":""},{"roles":"Secetary","candidateID":""},
		{"roles":"Welfare","candidateID":""},{"roles":"Sport Director","candidateID":""}
		];

	$scope.contestants = evotingService.fetchAllContestnts();
	console.log(JSON.stringify($scope.contestants))	
	$scope.sngContRoles;
	
	
	$scope.showDetails = function (){
		$("#ContestntList").hide();;
		$("#ContestntDetails").show();;
		$("#addNewContestnt").hide();;
		
	}

	$scope.addVote = function (contestntDet){
		var numOfVotedFor = 0;
		var breaKer=false;
		angular.forEach($scope.votedFor,function(v,key){		
            if(v.roles==contestntDet.tblCont.post){
				v.candidateID = contestntDet._id;
			}
			if(v.candidateID!=""){
				numOfVotedFor = numOfVotedFor +1;
			}
		});
		$scope.voteCount.count=numOfVotedFor;
		
	}

	

	$scope.castVote = function (){
		var breaker = false;
		var roleNotYetVotedFor;
		var indexofNotVer;
		alert(JSON.stringify($scope.votedFor))
		angular.forEach($scope.votedFor,function(v,key){		
			if(v.candidateID=="" && breaker==false){
				roleNotYetVotedFor = v.roles;
				indexofNotVer = key;
					breaker = true;
				}
			});
		if(breaker==true)
		{
			alert("Not Yet voted For "+roleNotYetVotedFor);
			$scope.filterContestant(roleNotYetVotedFor,indexofNotVer);
		}else{
			if(confirm("Are You Sure You want to cast Your Vote Now")){
				var resp = evotingService.saveCastedVote(JSON.stringify($scope.votedFor));
				alert("Thank You for Your Vote");
				window.location.href="audit.html";
			}
		}
		
		
		
	}


	
	$scope.addNewDetail = function (){
		$("#ContestntList").hide();;
		$("#ContestntDetails").hide();;
		$("#addNewContestnt").show();;
		
	}
	$scope.backToVoteList = function (){
		$("#ContestntList").show();;
		$("#ContestntDetails").hide();;
		$("#addNewContestnt").hide();;
		
	}

	$scope.backtoIndex = function (){
		navToLogin();
	}

	// function navToIndex(){
	// 	window.location.href="index.html";
	// }

	// function navToLogin(){
	// 	window.location.href="login.html";
	// }
	
	$scope.filterContestant = function (filterKey,filterID){
		$scope.sngContRoles = [];
		angular.forEach($scope.contestants ,function(v,key){
		
            if(v.tblCont.post==filterKey){
                $scope.sngContRoles.push(v);
            }
        });
		$scope.sngContRolesProp = {filterKey:filterID,"cont":$scope.sngContRoles};
	}

 });


app.controller('addContCntrl', function ($scope,evotingService) {
	// alert('well');
	setAddContestantView();

	$scope.roleOptions = evotingService.fetchRoles();
	$scope.userImg={"fileVal":""};

	$scope.showAddedList = function (){
		setContestantListView();		
	}
	$scope.showContDetails = function (contID){
		setContestantDetailView();
		$scope.contestDetail = $scope.contestants[contID];	
	}

	$scope.showAddContView = function (){
		setAddContestantView();
	}

	$scope.delContestnt = function(contID){
		if(!confirm("Do You want to Delete User?")){
				return;
		}
		// var resp = evotingService.deleteContestant(contID);
		alert(resp);
	}

	function setAddContestantView(){
		$("#addNewContestnt").show();;
		$("#ContestntList").hide();;
		$("#ContestntDetails").hide();;	
	}

	function setContestantDetailView(){
		$("#addNewContestnt").hide();;
		$("#ContestntList").hide();;
		$("#ContestntDetails").show();;	
	}

	function setContestantListView(){
		$("#addNewContestnt").hide();;
		$("#ContestntList").show();;
		$("#ContestntDetails").hide();;	
		$scope.contestants = evotingService.fetchAllContestnts();
	}

	$scope.subForm = function(contestDetail){
		contestDetail.imgVal = $scope.userImg;
		console.log(JSON.stringify(contestDetail));
		// $scope.fnlSub=  {"id":"1","tblContId":"2","tableDec":"votersTable","tblCont":contestDetail}
		console.log(JSON.stringify($scope.contestDetail));
		// var resp = evotingService.saveContDetails(JSON.stringify($scope.contestDetail));
		alert("Addded Successfully")
		$scope.contestDetail={};
		$scope.userImg.fileVal="";
	}

 });




 app.controller('settingsCntrl', function ($scope,evotingService) {
	 alert('cvbvsbgs');

});
app.controller('auditCntrl', function ($scope,evotingService) {
	$scope.viewSelected = {"data":""};
	$scope.resultView = 
	[
		{post:"President",contestants:['Adebayo Oji' , 'Mulikat Bimise'],
		result:[{value: 70,name:'Adebayo Oji'},{value: 10,name:'Mulikat Bimise'},{value: 50,name:'Vinod Shqakur'}]},
		{post:"V-President","contestants":['Adebayo Oji' , 'Mulikat Bimise'],
		result:[{value: 30,name:'Adebayo Oji'},{value: 10,name:'Mulikat Bimise'},
		{value: 30,name:'Adebayo Oji'},{value: 10,name:'Mulikat Bimise'},
		{value: 30,name:'Adebayo Oji'},{value: 10,name:'Mulikat Bimise'}]},
		{post:"PRO","contestants":['Adebayo Oji' , 'Mulikat Bimise'],
		result:[{value: 30,name:'Adebayo Oji'},{value: 10,name:'Mulikat Bimise'}]}
	];

	// $scope.scrLogic = {"val":$scope.resultView};
	$scope.voteResult = evotingService.fetchVoteResult();


	$scope.showDetail = function(postType){
		// alert("Showing for: "+postType)
		$scope.viewSelected.data = postType;
		$("#voteDetails").show();;
		$("#voteSummary").hide();;
	}
	$scope.showSummary = function(){
		$("#voteDetails").hide();;
		$("#voteSummary").show();;
	}
});


app.controller('signIn', function ($scope,evotingService) {
	$scope.loginCredential = {};
	$scope.signUpCredential = {};

	$scope.signIn = function(signInDetails){
		var resp = evotingService.SignIn(signInDetails.Username,signInDetails.password);
		if(resp=="Password Match"){
			alert("Welcome "+signInDetails.Username);
			window.location.href = "profile.html";
		}
		else if(resp=="No Record"){
			alert("Invalid Username or Password");
		}
		else{
			alert(resp);
		}
		
	}
	
	$scope.signUp = function(signUpDetails){		
		if(signUpDetails.password!=signUpDetails.password2){
			alert("Unmatched Password");
			return;
		}
		var resp = evotingService.SignUp(signUpDetails.Username,signUpDetails.password);
		//alert(resp);
		if(resp=="Successfull"){
			alert("Welcome "+signUpDetails.Username);
			window.location.href = "profile.html";
		}else if(resp=="User Found"){
			alert("User Already exists");
		}
	}
});


app.controller('addRole', function ($scope,evotingService) {
	showRoleListView();
	
	$scope.roledata = {roleName:"",roleDesc:"",roleAddedBy:"Admin",roleCreatedate:0,numOfcontestant:0};
	$scope.roles;

	$scope.showAddedList = function (){
		showRoleListView();
		
	}

	$scope.showRoleDetails = function (roleDetails){
		showRoleDetails();
		$scope.roledata = roleDetails;
		
	}

	$scope.showAddRoleView = function (){
		showAddRoleView();
		$scope.roledata = {};
		
	}


	$scope.addRole = function (){
		$scope.roledata.roleCreatedate = 12331313131;
		$scope.roledata.numOfcontestant = 0;
		$scope.roleAddedBy = "Admin";
		if($scope.roledata.roleName==""){
			alert("RoleName Cannot Be empty");
			return ;
		}else{
			var addStatus = evotingService.saveRole($scope.roledata);
			$scope.roledata = {};
		}
	}

	$scope.deleteRole = function(contID){
		if(!confirm("Do You want to Delete User?")){
				return;
		}
		var resp = evotingService.deleteContestant(contID);
		alert(resp);
	}

	function showRoleListView(){
		$("#addrole").hide();;
		$("#roleList").show();;
		$("#roleDetails").hide();;	
		$scope.roles = evotingService.fetchRoles();
	}

	function showAddRoleView(){
		$("#addrole").show();;
		$("#roleList").hide();;
		$("#roleDetails").hide();;	
	}

	function showRoleDetails(){
		$("#addrole").hide();;
		$("#roleList").hide();;
		$("#roleDetails").show();;	
	}

 });




app.controller('settingsCntrl', function ($scope,evotingService) {
	 alert('cvbvsbgs');

});
 
app.directive("fileread", [function () {
		return {
			scope: {
				fileread: "="
			},
			link: function (scope, element, attributes) {
				element.bind("change", function (changeEvent) {
					var reader = new FileReader();
					reader.onload = function (loadEvent) {
						scope.$apply(function () {
							scope.fileread = [];
							scope.fileread2 = loadEvent.target.result;
							scope.fileread.push(scope.fileread2);
							//alert(JSON.stringify(scope.fileread[0]));
						});
					}
					reader.readAsDataURL(changeEvent.target.files[0]);
					
				});
				
			}
		}
	}]);

	function navToIndex(){
		
		window.location.href="index.html";
	}

	function navToLogin(){
		window.location.href="login.html";
		alert("Welcoe Man");
	}